import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SportEventsComponent } from './sport-events.component';
import { routing } from 'src/app/routes/app.routing';
import { SportEventsService } from 'src/app/services/sport-events.service';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ng6-toastr-notifications';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';
import { doesNotThrow } from 'assert';

describe('SportEventsComponent', () => {
  let component: SportEventsComponent;
  let fixture: ComponentFixture<SportEventsComponent>;
  let el: HTMLElement;
  let de: DebugElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SportEventsComponent ],
      imports: [
        routing,
        FormsModule,
        HttpClientModule,
        ToastrModule.forRoot()
      ],
      providers: [ {
        provide: APP_BASE_HREF, useValue: '/'
      }]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SportEventsComponent);
    component = fixture.componentInstance;
    de = fixture.debugElement.query(By.all());
    el = de.nativeElement;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it(`should have button with text 'Edit mode'`, () => {
    expect(component.btnText.includes('Edit mode'));
  });

  it(`should set isPreviewMode to TRUE`, () => {
    expect(component.isPreviewMode.valueOf() === true);
  });

  it(`should have one sport event`, () => {
    component.sportEvents$.subscribe(data => {
      expect(data.length).toBeGreaterThan(0);
    });

  });
});
