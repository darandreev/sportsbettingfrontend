import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { SportEventsService } from './sport-events.service';
import { HttpClientModule } from '@angular/common/http';
import { SportEventsComponent } from '../components/sport-events/sport-events.component';
import { environment } from 'src/environments/environment';
import { SportEvent } from '../models/SportEvent';

describe('SportEventsService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SportEventsService],
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ]
    });
  });

  it('should be created', inject([SportEventsService], (service: SportEventsService) => {
    expect(service).toBeTruthy();
  }));

  it(`should get data of sport events`, () => {
    const sportEvent = {
      eventID: 1,
      eventName: 'Juventus-Barcelona',
      eventStartDate: new Date()
    };

    const sportEventService = TestBed.get(SportEventsService);
    const http = TestBed.get(HttpTestingController);
    let response: any;
    sportEventService.getAllSportEvents().subscribe(data => {
        response = data;
    });

    http.expectOne(environment.apiURL).flush(sportEvent);

    expect(response).toEqual(sportEvent);
  });

  it(`add sport event`, () => {
    let fakeResponse = null;
    const sportEvent: SportEvent = {
      eventID: 1,
      eventName: 'Juventus-Barcelona',
      oddsForDraw: 1,
      oddsForFirstTeam: 1,
      oddsForSecondTeam: 1,
      eventStartDate: new Date()
    };

    const sportEventService = TestBed.get(SportEventsService);

    sportEventService.addSportEvent(sportEvent).subscribe(data => {
      fakeResponse = data;
    });

    expect(fakeResponse).toBeDefined();

  });
});
