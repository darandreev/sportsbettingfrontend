import { Component, OnInit } from '@angular/core';
import { SportEventsService } from 'src/app/services/sport-events.service';
import { SportEvent } from 'src/app/models/SportEvent';
import { DatePipe } from '@angular/common';
import { Observable } from 'rxjs';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-sport-events',
  templateUrl: './sport-events.component.html',
  styleUrls: ['./sport-events.component.scss']
})
export class SportEventsComponent implements OnInit {

  public sportEvents$: Observable<SportEvent[]>;
  public sportEvent: SportEvent;
  public isPreviewMode = true;
  public btnText = 'Edit mode';

  constructor(private sportEventsService: SportEventsService,
              private toastr: ToastrManager) {
    this.sportEvents$ = new Observable<SportEvent[]>();
    this.sportEvent = new SportEvent();
  }

  ngOnInit() {
    this.getSportEvents();
  }

  getSportEvents() {
    this.sportEvents$ = this.sportEventsService.getAllSportEvents();
  }

  addSportEvent() {
    this.sportEvent.eventStartDate = new Date();
    this.sportEvent.eventStartDate.setHours(23);
    this.sportEvent.eventStartDate.setMinutes(59);

    this.sportEventsService.addSportEvent(this.sportEvent)
        .subscribe(() => {
          this.getSportEvents();
          this.toastr.successToastr('Sport event added successfully!');
        },
        (error) => {
            this.toastr.errorToastr('Error while adding event!', error);
        });
  }

  deleteSportEvent(sportEvent: SportEvent) {
    this.sportEventsService.deleteSportEvent(sportEvent.eventID)
          .subscribe(event => {
            this.getSportEvents();
          },
          (error) => {
            this.toastr.errorToastr(error, 'Error!');
          });
  }

  updateSportEvent(sportEvent: SportEvent) {
    this.sportEventsService.updateSportEvent(sportEvent)
          .subscribe(data => {
            this.getSportEvents();
          }, (error) => {
            this.toastr.errorToastr(error, 'Error!');
          });
  }

  changeMode() {
    this.isPreviewMode = !this.isPreviewMode;

    if (!this.isPreviewMode) {
      this.btnText = 'Preview mode';
    } else {
      this.btnText = 'Edit mode';
    }
  }

  changeRowColor(sportEvent: SportEvent, index: number) {
    const now = new Date();
    const startDate = new Date(sportEvent.eventStartDate);
      if (this.isPreviewMode) {
        if (startDate.getTime() < now.getTime()) {
          return 'row-passed';
        } else {
          if (index % 2 === 0) {
            return 'row-even';
          } else {
            return 'row-odd';
          }
        }
    } else {
      return 'row-default';
    }

  }

  oddClick(sportEvent: SportEvent, event) {
      if (this.isPreviewMode) {
        if (event.srcElement.innerHTML.includes('oddsForFirstTeam')) {
          console.log(sportEvent.eventID + ' - ' + 'OddsForFirstTeam' + ' - ' + sportEvent.oddsForFirstTeam);
        } else if (event.srcElement.innerHTML.includes('oddsForDraw')) {
          console.log(sportEvent.eventID + ' - ' + 'oddsForDraw' + ' - ' + sportEvent.oddsForDraw);
        } else {
          console.log(sportEvent.eventID + ' - ' + 'oddsForSecondTeam' + ' - ' + sportEvent.oddsForSecondTeam);
        }
    }
  }

}
