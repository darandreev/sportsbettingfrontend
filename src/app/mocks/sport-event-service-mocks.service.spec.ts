import { TestBed, inject } from '@angular/core/testing';

import { SportEventServiceMocksService } from './sport-event-service-mocks.service';

describe('SportEventServiceMocksService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SportEventServiceMocksService]
    });
  });

  it('should be created', inject([SportEventServiceMocksService], (service: SportEventServiceMocksService) => {
    expect(service).toBeTruthy();
  }));
});
