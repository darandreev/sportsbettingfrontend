import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { routing } from './routes/app.routing';
import { SportEventsComponent } from './components/sport-events/sport-events.component';
import { FormsModule } from '@angular/forms';
import { APP_BASE_HREF } from '@angular/common';
describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        SportEventsComponent
      ],
      imports: [
        routing,
        FormsModule
      ],

      providers: [ {
        provide: APP_BASE_HREF, useValue: '/'
      }]
    }).compileComponents();
  }));
  it('should create the app', async(() => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  }));

});
