export class SportEvent {
    eventID: number;
    eventName: string;
    eventStartDate: Date;
    oddsForDraw: number = 1.0;
    oddsForSecondTeam: number = 1.0;
    oddsForFirstTeam: number = 1.0;

}
