import { BrowserModule } from '@angular/platform-browser';
import { NgModule, forwardRef } from '@angular/core';
import { AppComponent } from './app.component';
import { SportEventsComponent } from './components/sport-events/sport-events.component';
import { routing } from './routes/app.routing';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { SportEventsService } from './services/sport-events.service';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, NG_VALUE_ACCESSOR } from '@angular/forms';
import { ToastrModule } from 'ng6-toastr-notifications';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_BASE_HREF } from '@angular/common';

@NgModule({
  declarations: [
    AppComponent,
    SportEventsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    routing,
    ToastrModule.forRoot(),
    MDBBootstrapModule.forRoot()
  ],
  providers: [SportEventsService, {
    provide: APP_BASE_HREF, useValue: '/'
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
