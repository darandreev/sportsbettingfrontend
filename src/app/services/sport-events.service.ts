import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { SportEvent } from '../models/SportEvent';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { catchError, retry } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SportEventsService {

  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json'
    })
  };

  constructor(private http: HttpClient) { }

  getAllSportEvents(): Observable<SportEvent[]> {
    return this.http
            .get<SportEvent[]>(environment.apiURL)
            .pipe(
              retry(3),
              catchError(this.handleError)
            );
  }

  addSportEvent(sportEvent: SportEvent): Observable<SportEvent> {
    return this.http
            .post<SportEvent>(environment.apiURL, sportEvent, this.httpOptions);
  }

  deleteSportEvent(sportEventId: number): Observable<SportEvent> {
    return this.http
              .delete<SportEvent>(environment.apiURL + sportEventId);
  }

  updateSportEvent(sportEvent: SportEvent): Observable<SportEvent> {
    return this.http
              .put<SportEvent>(environment.apiURL + sportEvent.eventID, sportEvent);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
