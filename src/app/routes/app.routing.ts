import { RouterModule, Routes } from '@angular/router';
import { ModuleWithProviders } from '@angular/compiler/src/core';
import { SportEventsComponent } from '../components/sport-events/sport-events.component';


export const routes: Routes = [
    { path: '', redirectTo: 'sportevents', pathMatch: 'full'},
    { path: 'sportevents', component: SportEventsComponent},
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes);
